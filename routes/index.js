const Powershell = require('powershell')
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Register'});
});

// Authenticate
router.post('/login', (req, res) => {
    let firstLetterCap = req.body.name.charAt(0).toUpperCase()
    let samName = firstLetterCap + ". " + req.body.surname
    let cmd = `New-ADUser -Name "${req.body.name} ${req.body.surname}"` +
        `-GivenName "${req.body.name}" `+
        `-Surname "${req.body.surname}" `+
        `-SamAccountName "${samName}" `+
        `-UserPrincipalName "${samName}@ehh.de" `+
        `-Path "OU=Guest,DC=ehh,DC=de" ` +
        `-AccountPassword(ConvertTo-SecureString -AsPlainText -Force "${req.body.password}") `+
        `-Enabled $true`;
    let ps = new Powershell(cmd)
    console.log(cmd)
    ps.on("error", err => {
        console.error(err);
    });
    ps.on("output", data => {
        console.log(data);
    });
    ps.on("error-output", data => {
        console.error(data);
    });
    ps.on("end", code => {
        if (code === 0) {
            res.send("Registration Failed")
        } else {
            res.send("Registration Successful")
        }
    });
})

module.exports = router;
